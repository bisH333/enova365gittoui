﻿using Soneta.Business;
using enova365.GitToUI.GitToUI.Extender;
using Soneta.Business.UI;
using Soneta.Types;

[assembly: Worker(typeof(GitToUI))]

namespace enova365.GitToUI.GitToUI.Extender
{
    public partial class GitToUI
    {
        [Context]
        public Context Context { get; set; }

        private string url;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        private string path;

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        private Date data;

        public Date Data
        {
            get { return data; }
            set { data = value; }
        }

        private string osoba;

        public string Osoba
        {
            get { return osoba; }
            set { osoba = value; }
        }

        public object GetListOsoba()
        {
            return getUsers;
        } 

        public int IlDzien =>IloscNaDzien(osoba, data);

        public float IlSrednia => IloscSrednia(osoba, Dni(osoba).Count);

        public MessageBoxInformation Pokaz()
        {
            return new MessageBoxInformation("Wyświetlanie commitów", "Czy chcesz wyświetlić podsumowanie commitów?")
            {
                YesHandler = () =>
                {
                    Czysc();
                    CloneRepository();
                    GetCommits();
                    Przelicz();
                    Context.Session.InvokeChanged();
                    return null;
                }
            };
        }
    }
}
