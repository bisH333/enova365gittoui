﻿using Soneta.Types;

namespace enova365.GitToUI.GitToUI.Extender
{
    public class IloscNaDzien
    {
        public string Osoba { get; set; }
        public Date Data { get; set; }
        public int Ilosc { get; set; }
    }
}
