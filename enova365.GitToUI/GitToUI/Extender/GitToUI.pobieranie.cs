﻿using LibGit2Sharp;
using Soneta.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace enova365.GitToUI.GitToUI.Extender
{
    public partial class GitToUI
    {
        List<Signature> commits = new List<Signature>();
        List<IloscNaDzien> ilosciNaDzien = new List<IloscNaDzien>();
        List<IloscSrednia> ilosciSrednie = new List<IloscSrednia>();
        private string[] getUsers = new string[0];

        private void GetCommits()
        {
            using (Repository git = new Repository(path))
            {
                foreach (var item in git.Branches)
                    foreach (Commit commit in item.Commits)
                        commits.Add(commit.Committer);
            }
            commits = commits.Distinct().OrderBy(c => c.When).ToList();
            getUsers = commits.Select(c => c.Name).Distinct().ToArray();
        }

        private int IloscNaDzien(string osoba, DateTime data) => commits.Count(c => c.Name == osoba && c.When.Date == data);

        private float IloscSrednia(string osoba, int iloscDni) => (float)commits.Count(c => c.Name == osoba) / (float)iloscDni;

        public string[] GetUsers
        {
            get => getUsers;
            private set => getUsers = value;
        }

        public IEnumerable<IloscSrednia> IlosciSrednie => ilosciSrednie;

        public IEnumerable<IloscNaDzien> IlosciNaDzien => ilosciNaDzien;

        private void Przelicz()
        {
            foreach (var user in GetUsers)
            {
                List<DateTime> dni = Dni(user);
                foreach (var dzien in dni)
                {
                    ilosciNaDzien.Add(new IloscNaDzien() { Osoba = user, Data = dzien, Ilosc = IloscNaDzien(user, dzien) });
                }
                ilosciSrednie.Add(new IloscSrednia() { Osoba = user, Ilosc = IloscSrednia(user, dni.Count) });
            }
        }

        private List<DateTime> Dni(string user) => commits.Where(c => c.Name == user).Select(c => c.When.Date).Distinct().ToList();

        private void Czysc()
        {
            commits = new List<Signature>();
            ilosciNaDzien = new List<IloscNaDzien>();
            ilosciSrednie = new List<IloscSrednia>();
            getUsers = new string[0];
            osoba = "";
            data = Date.Empty;
        }
    }
}


