﻿using enova365.GitToUI.GitToUI.Extender;
using Soneta.Business.Licence;
using Soneta.Business.UI;

[assembly: FolderView("GIT",
    Priority = 110,
    Description = "Git...",
    BrickColor = FolderViewAttribute.BlueBrick,
    Icon = "git.ico;enova365.GitToUI",
    Contexts = new object[] { LicencjeModułu.All }
)]

[assembly: FolderView("GIT/Podgląd commitów",
    Priority = 111,
    Description = "Przeglądanie commitów ze wskazanego repozytorium",
    ObjectType = typeof(GitToUI),
    ObjectPage = "GitToUI.ogolne.pageform.xml",
    ReadOnlySession = false,
    ConfigSession = false
)]