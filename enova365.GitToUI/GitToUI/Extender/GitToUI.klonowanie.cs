﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace enova365.GitToUI.GitToUI.Extender
{
    public partial class GitToUI
    {
        private void CloneRepository()
        {
            if (!string.IsNullOrEmpty(url))
                path = Repository.Clone(url, path);
        }
    }
}
